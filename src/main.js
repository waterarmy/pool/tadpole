const { app, session, ipcMain, BrowserWindow } = require('electron')
const path = require('path')

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  app.quit()
}

const handleAskPath = async () => {
  return {
    dirname: __dirname,
    cwd: process.cwd(),
    cwd_resolve: path.resolve(process.cwd()),
    resolve: path.resolve('./'),
    app: app.getAppPath(),
    exe: app.getPath('exe'),
  }
}

const registerWindowMap = {}

const createWindow = () => {
  const mainWindow = new BrowserWindow({
    x: 40,
    y: 40,
    width: 360,
    height: 360,
    // transparent: true,
    frame: false,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      // contextIsolation: false,
    },
  })

  const subWindow = new BrowserWindow({
    x: 440,
    y: 40,
    width: 360,
    height: 360,
    // transparent: true,
    frame: false,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      // contextIsolation: false,
    },
  })

  mainWindow.loadFile(path.join(__dirname, 'index.html'))
  subWindow.loadFile(path.join(__dirname, 'index.html'))

  session.defaultSession.webRequest.onHeadersReceived((details, callback) => {
    callback({
      responseHeaders: {
        ...details.responseHeaders,
        'Content-Security-Policy': ["default-src 'self'"],
      },
    })
  })

  ipcMain.handle('ask-path', handleAskPath)

  mainWindow.webContents.openDevTools({ mode: 'detach' })
  subWindow.webContents.openDevTools({ mode: 'detach' })

  mainWindow.webContents.send('name', 'main')
  subWindow.webContents.send('name', 'sub')

  registerWindowMap['main'] = mainWindow.id
  registerWindowMap['sub'] = subWindow.id
}

ipcMain.on('log', (event, arg) => {
  console.log('log', event.sender.id, arg)
})

ipcMain.on('sync-register-window-map', (event, arg) => {
  event.returnValue = registerWindowMap
})

ipcMain.on('sync-register-window-name', (event) => {
  const id = event.sender.id
  for (let key in registerWindowMap) {
    if (registerWindowMap[key] === id) {
      event.returnValue = key
      break
    }
  }
})

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})
