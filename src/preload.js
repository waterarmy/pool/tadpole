// See the Electron documentation for details on how to use preload scripts:
// https://www.electronjs.org/docs/latest/tutorial/process-model#preload-scripts

const { contextBridge, ipcRenderer } = require('electron')

let name = ''

function getName() {
  if (name) {
    return name
  } else {
    return ipcRenderer.sendSync('sync-register-window-name')
  }
}

function log(payload) {
  const name = getName()
  ipcRenderer.send('log', payload ? { from: name, payload } : { from: name })
}

contextBridge.exposeInMainWorld('myAPI', {
  getProcess: () => process,
  askPath: () => ipcRenderer.invoke('ask-path'),
})

contextBridge.exposeInMainWorld('ipc', {
  send: (...args) => ipcRenderer.send(...args),
  sendSync: (...args) => ipcRenderer.sendSync(...args),
  sendTo: (...args) => ipcRenderer.sendTo(...args),
  on: (...args) => ipcRenderer.on(...args),
  get name() {
    return getName()
  },
  log,
})

ipcRenderer.on('name', (event, arg) => {
  console.log('I am ' + arg, Date.now())
  name = arg
  log()
})

ipcRenderer.on('ping', (event, arg) => {
  console.log('ping', JSON.stringify(arg))
  log(arg)
})
